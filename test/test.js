var supertest = require("supertest");
var mocha = require('mocha');
var reqData=require('../requestData.json');
var chai = require('chai');
const { expect } = require("chai");
chai.should();

var server = supertest.agent(reqData.host);
var mocha = new mocha({reporter: 'mochawesome'});

describe(reqData.heading, function(){  
    reqData.TC_Obj.forEach(function(TC_Obj){
        it(TC_Obj.title,function(done){
            server
            .post(TC_Obj.path)
            .send(TC_Obj.body)
            .set(TC_Obj.header)
            .expect(function(res){
                let resObj = JSON.parse(res.text);
                if (resObj.hasOwnProperty('status')) {
                    resObj.status.should.equal("success");  
                    }   
            })
            .end(done)
        });
    })       
});